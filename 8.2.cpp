#include <iostream>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
using namespace std;

// есть матрица до 100. найти строку сумма элементов которой больше близка к 0 и изменить все  числа в этой строке на 0;

int main()
{
    cout<<"Введите размер матрицы:"<<endl;
    int n,m;
    cin>>n;//количество строк i
    cout<<"на"<<endl;
    cin>>m;//количество столбцов j
    int **mas = new int* [n];
    for(int i =0; i < n; i++)
        mas[i] = new int [m];
    int Sumof_Strok[n];
    int Sum=0;
    for(int i = 0;i<n;i++)
        for(int j =0 ; j < m ;j++)
            cin>>mas[i][j];
    for(int i = 0;i<n;i++) //цикл для подсчета сумм ячеек каждой строки
    {
        for(int j =0 ; j < m ;j++)
        {
            Sum += mas[i][j];
            if(j == m-1)
            {
                Sumof_Strok[i] = Sum;
                Sum = 0;
            }
        }
    }
    int MinOf_Sum;//минимальная сумма
    int min=1000;
    int Min_Str;//строка минимальной суммы
    for(int i = 0;i<n;i++)//цикл для вычисления минимальной суммы и строки этой суммы
    {
        if(Sumof_Strok[i] < min)
        {
            MinOf_Sum=Sumof_Strok[i];
            min = MinOf_Sum;
            Min_Str=i;
        }

    }
    for(int i = 0 ; i < n;i++) //цикл для замены в матрице наименьшей строки на нулевую
    {
        if( i == Min_Str)
        {
            for(int j =0 ; j < m ;j++)
            {
                mas[i][j]=0;
            }
        }
    }
    for(int i = 0;i<n;i++)//вывод матрицы с нулевой строкой
    {
        for(int j =0 ; j < m ;j++)
            cout<<mas[i][j]<<' ';
        cout<<endl;
    }

    for(int i = 0 ; i < n ;i++)
        delete [] mas[i];
    delete [] mas;
    return 0;
}
