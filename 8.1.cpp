#include <iostream>
#include <cmath>
#include <string>
#include <sstream>
#include <fstream>
using namespace std;

//8 лаба динамическая память 2 и 3 задание

//задание №2

//по возрастанию первой цифры числа в массиве----1
//если цифра первая одинаковая то по возрастанию суммы цифр числа----2
//если и сумма цифр и первая цифра числа одинаковые, то по возрастанию самого числа-----3
// количество чисел: 10
// 5 33 10 31 21 97 165 156 15 200
//----1) 10 165 156 15 21 200 33 31 5 97
//----2) 10 15 165 156 200 21 31 33 5 97
//----3) 10 15 156 165 200 21 31 33 5 97


int main()
{
    cout<<"Введите количество чисел: ";
    int a = 0;//количество чисел
    int temp1,temp2;//переменная которая будет хранить число переходящее
    cin>>a;
    cout<<"Введите числа: ";
    int *mas = new int [a];//основной массив
    int *arr = new int [a];//массив первой цифры числа
    int SumOf_Cif1 = 0; // сумма цифр числа
    int SumOf_Cif2 = 0; // сумма цифр числа
    int b;
    int c;

    for(int i =0; i < a; i++)//ввод с клавиатуры массива чисел размером "a"
        cin>>mas[i];

    for(int i =0; i < a ; i++)
        arr[i]=mas[i];

    for(int i =0 ;i<a;i++)// меняем в массиве arr число на его первую цифру
    {
        while(arr[i]>9)
            arr[i]=arr[i]/10;
    }

    for(int i = 0; i < a - 1;i++)// сортировка по первому условию
    {
        for(int j = i+1; j < a;j++)
        {
            if(arr[i] > arr[j])
            {
                temp1 = mas[i];
                temp2 = arr[i];//проверка
                mas[i] = mas[j];
                arr[i] = arr[j];//проверка
                mas[j]=mas[i+1];
                arr[j]=arr[i+1];//проверка
                mas[i+1]=temp1;
                arr[i+1]=temp2;//проверка
            }
        }
    }

    for(int i = 0; i < a - 1;i++)// сортировка по второму условию
    {
        for (int j = i + 1; j < a; j++)
        {
            SumOf_Cif1 = 0;
            SumOf_Cif2 = 0;
            if (arr[i] == arr[j])
            {
                b = mas[i];
                c = mas[j];
                while (b != 0)
                {
                    SumOf_Cif1 += b % 10;
                    b = b / 10;
                }
                while (c != 0)
                {
                    SumOf_Cif2 += c % 10;
                    c = c / 10;
                }
                if ( SumOf_Cif1 > SumOf_Cif2)
                {
                    temp1 = mas[i];
                    temp2 = arr[i];//проверка
                    mas[i] = mas[j];
                    arr[i] = arr[j];//проверка
                    mas[j] = mas[i + 1];
                    arr[j] = arr[i + 1];//проверка
                    mas[i + 1] = temp1;
                    arr[i + 1] = temp2;//проверка
                }
                else if(SumOf_Cif1 == SumOf_Cif2)
                {
                    if(mas[i] > mas[j])
                        swap(mas[i],mas[j]);
                }
            }
        }
    }

    cout<<"Выполненная сортировка: ";
    for(int i =0 ;i<a;i++)//проверка конечного условия
        cout<<*(mas+i)<<' ';
    delete [] arr;
    delete [] mas;
    return 0;
}

